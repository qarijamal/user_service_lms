package com.lms.user_service.dtos;

public class UserDTO {
    private Long id;
    private String name;
    private String cnic;
    private String mobile;
    private String address;

    public UserDTO() {
    }

    public UserDTO(Long id, String name, String mobile, String address, String cnic) {
        this.id = id;
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.cnic = cnic;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
