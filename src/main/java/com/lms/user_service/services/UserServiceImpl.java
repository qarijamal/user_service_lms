package com.lms.user_service.services;

import com.lms.user_service.dtos.UserDTO;
import com.lms.user_service.entities.User;
import com.lms.user_service.exception.UserException;
import com.lms.user_service.exception.UserNotFoundException;
import com.lms.user_service.repos.UserRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserManager {

    @Autowired
    private UserRepo userRepo;

    @Override
    public String saveUser(UserDTO userDTO) {
        User user = new ModelMapper().map(userDTO, User.class);
        this.userRepo.save(user);
        return "";
    }

    @Override
    public String updateUser(UserDTO userDTO) {
        User user2 = this.userRepo.getUserById(userDTO.getId());
        if (user2 == null) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(),"User not found");
        }
        User user = new ModelMapper().map(userDTO, User.class);
        this.userRepo.save(user);
        return "";
    }

    @Override
    public String deleteUser(long id) throws UserNotFoundException {
        User user = this.userRepo.getUserById(id);
        if (user == null) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(), "User not found");
        }
        this.userRepo.delete(user);
        return "";
    }

    @Override
    public UserDTO getUser(long id) throws UserNotFoundException {
        User user = this.userRepo.getUserById(id);
        if (user == null) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(), "User not found");
        }
        UserDTO userDTO = new UserDTO(user.getId(), user.getName(), user.getMobile(), user.getAddress(), user.getCnic());
        return userDTO;
    }

    @Override
    public boolean isUserExistByCNIC(String cnic) {
        User user = this.userRepo.getUserByCnic(cnic);
        if (user != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isUserExistByCNICAndId(long id, String cnic) {
        User user = this.userRepo.getUserByIdAndCnic(id, cnic);
        if (user != null) {
            return true;
        }
        return false;
    }

    @Override
    public List<UserDTO> getUsers() throws UserNotFoundException {
        List<User> users = this.userRepo.findAll();
        if (users == null) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(), "Users not available.");
        }
        List<UserDTO> userDTOS = new ArrayList<>();
        for (User user : users) {
            userDTOS.add(new ModelMapper().map(user, UserDTO.class));
        }
        return userDTOS;
    }
}
