package com.lms.user_service.services;

import com.lms.user_service.dtos.UserDTO;
import com.lms.user_service.exception.UserException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserManager {

    String saveUser(UserDTO userDTO) throws UserException;

    String updateUser(UserDTO userDTO) throws UserException;

    String deleteUser(long id) throws UserException;

    UserDTO getUser(long id) throws UserException;

    boolean isUserExistByCNIC(String cnic) throws UserException;

    boolean isUserExistByCNICAndId(long id, String cnic) throws UserException;

    List<UserDTO> getUsers() throws UserException;
}
