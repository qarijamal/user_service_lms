package com.lms.user_service.controllers;


import com.lms.user_service.dtos.UserDTO;
import com.lms.user_service.exception.UserAlreadyExistException;
import com.lms.user_service.exception.UserNotFoundException;
import com.lms.user_service.services.UserManager;
import com.lms.user_service.vo.UserVO;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
public class UserController {

    public static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserManager userManager;

    @PostMapping("user")
    public ResponseEntity<?> saveUser(@RequestBody UserVO userVO) throws UserAlreadyExistException {
        logger.info("saveUser called.......");
        if (this.userManager.isUserExistByCNIC(userVO.getCnic())) {
            throw new UserAlreadyExistException(HttpStatus.CONFLICT.value(), "User already exists");
        }
        this.userManager.saveUser(new ModelMapper().map(userVO, UserDTO.class));
        logger.info("saveUser ended.......");
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("user/{id}")
    public ResponseEntity<?> getUser(@PathVariable("id") long id) {
        logger.info("getUser called.......");
        UserDTO userDTO = this.userManager.getUser(id);
        logger.info("getUser ended.......");
        return new ResponseEntity(userDTO, null, HttpStatus.OK);
    }

    @PutMapping("user")
    public ResponseEntity<?> updateUser(@RequestBody UserVO userVO) throws UserNotFoundException {
        logger.info("updateUser called.......");
        if (userVO.getId() == null || userVO.getId() <= 0) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(), "Please provide user Id");
        }
        if (!this.userManager.isUserExistByCNICAndId(userVO.getId(), userVO.getCnic())) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(), "User not found.Please provide cnic.");
        }
        this.userManager.updateUser(new ModelMapper().map(userVO, UserDTO.class));
        logger.info("updateUser ended.......");

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("user/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") long id) throws UserNotFoundException {
        logger.info("deleteUser  called.......");

        if (id <= 0) {
            throw new UserNotFoundException(HttpStatus.NOT_FOUND.value(), "Please provide user Id");
        }
        this.userManager.deleteUser(id);
        logger.info("deleteUser  ended.......");
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("user/all")
    public ResponseEntity<?> getUsers() {
        logger.info("getUsers called.......");
        return new ResponseEntity(this.userManager.getUsers(), null, HttpStatus.OK);
    }

}
