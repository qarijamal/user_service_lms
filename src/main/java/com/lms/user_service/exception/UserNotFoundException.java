package com.lms.user_service.exception;

public class UserNotFoundException extends UserException {
    public UserNotFoundException(int code, String message) {
        super(code, message);
    }
}
