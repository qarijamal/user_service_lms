package com.lms.user_service.exception;

public class UserAlreadyExistException extends UserException {
    public UserAlreadyExistException(int code, String message) {
        super(code, message);
    }
}
