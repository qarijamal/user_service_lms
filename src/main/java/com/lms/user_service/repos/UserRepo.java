package com.lms.user_service.repos;

import com.lms.user_service.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Long> {

    User getUserByCnic(String cnic);

    User getUserByIdAndCnic(long id, String cnic);

    User getUserById(long id);
}
