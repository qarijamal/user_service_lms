/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.25-log : Database - user_service_lms_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`user_service_lms_db` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `user_service_lms_db`;

/*Table structure for table `hibernate_sequence` */

DROP TABLE IF EXISTS `hibernate_sequence`;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `hibernate_sequence` */

insert  into `hibernate_sequence`(`next_val`) values (33),(33),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1),(1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cnic` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`address`,`mobile`,`name`,`cnic`) values (1,'LHR','321654987','JAMAL','1111'),(2,'LHR','22222222222','jamal','222'),(3,'LHR 44444444444444','4444444444','jamal','333'),(5,'sadfasdfs asdf adf a','978645312','jamal','55'),(12,'Address 1549542453855','1549542455496','Qari Muhammad Jamal1549542453428','1549542454190'),(11,'sadfasdfs asdf adf a','978645312','jamal','321654987'),(13,'Address 1549542571137','1549542571137','Qari Muhammad Jamal1549542571137','1549542571137'),(14,'Address 1549542824020','1549542824020','Qari Muhammad Jamal1549542824020','1549542824020'),(19,'Address 1549548039446','1549548039446','Qari Muhammad Jamal1549548039446','1549548039446'),(16,'Address 1549542932696','1549542932696','Qari Muhammad Jamal1549542932696','1549542932696'),(17,'Address 1549543120279','1549543120279','Qari Muhammad Jamal1549543120279','1549543120279'),(18,'bwp','9786453121','jamal1','3216549871'),(25,'bwp 1111111111111111111','1111111111111111111111111111','jamal1','1111111111111111111111111'),(23,'bwp 9999999999999','9999999999999999','jamal1','999999999'),(21,'LHR 2020202 0202020 ','202020202020202','jamal','1549550883521'),(24,'bwp 101010101','101010101','jamal1','101010101010'),(26,'bwp 22222222222222','22222222222222222222','jamal1','2222222222222222'),(27,'Address 1549551647586','1549551647586','Qari Muhammad Jamal1549551647586','1549551647586'),(29,'bwp masjid khizra','03216829613','Qari Muhammad Jamal','3120288222725'),(30,'Address 1549554773563','1549554773563','Qari Muhammad Jamal1549554773563','1549554773563'),(31,'Address 1549554886960','1549554886960','Qari Muhammad Jamal1549554886960','1549554886960'),(32,'Address 312028822272522 updated','1549551829408 312028822272522','Qari Muhammad Jamal1549551829408 updated','312028822272522');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
