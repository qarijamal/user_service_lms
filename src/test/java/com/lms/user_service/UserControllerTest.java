package com.lms.user_service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lms.user_service.controllers.UserController;
import com.lms.user_service.dtos.UserDTO;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static com.lms.user_service.utils.UtilUser.jsonString;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserServiceLMSApplication.class)
public class UserControllerTest {
    private static String BASE_URL = "/api/";
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void saveUserAsNewTest() throws Exception {
        UserDTO userDto = new UserDTO();
        userDto.setName("Qari Muhammad Jamal" + new Date().getTime());
        userDto.setAddress("Address " + new Date().getTime());
        userDto.setCnic(new Date().getTime() + "");
        userDto.setMobile(new Date().getTime() + "");
        mockMvc.perform(
                post(BASE_URL + "user")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(jsonString(userDto)))
                .andExpect(status().isCreated())
        ;
    }


}
